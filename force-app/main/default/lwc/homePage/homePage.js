import { LightningElement, track, api, wire } from 'lwc';
import insertStudent from "@salesforce/apex/StudentHandler.insertStudent";
import getStudentFieldSetMemebers from "@salesforce/apex/StudentHandler.getStudentFieldSetMemebers";
import studentDetails from "@salesforce/apex/StudentHandler.studentDetails";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';

export default class HomePage extends NavigationMixin(LightningElement) {

    @api recordId;
    @api objectApiName;
    @api objectName;
    @api fieldSetName;
    @track objStudent = {};
    @track studentClone = {}
    @track studentCloneTwo = {};
    @track lstStudentTwo = {};
    @track lstFields = [];
    @track buttonstatus = false ;
    @track Name;
    @track fieldWrapper = {};
    @track buttonLabel;

    connectedCallback() {  // ConnectedCallback function to call the fieldSetMembers while  the page loads 
        this.fieldSetMember();
        
        if(this.recordId == null)
        {
            this.buttonstatus = false;
            this.buttonLabel = "SAVE"
        }
        else if( this.recordId != null )
        {
            this.buttonstatus = true;
            this.buttonLabel = "EDIT"
        }
        
    }

    renderedCallback() {  //RenderedCallBack to fetch the accounts . 
        this.fetchStudentRecord();
    }
    fieldSetMember() { // This function retrieves the fields which are set in the field set and displays in the form .
        getStudentFieldSetMemebers({
            objectApiName: this.objectName,
            fieldsetApiName: this.fieldSetName
        }).then(res => {
            this.lstFields = res;
            for (var i = 0; i < res.length; i++) {
                if (res[i].type == "BOOLEAN") {
                    res[i].type = "checkbox";
                    res[i].variant = "label-inline";
                }
                else if (res[i].type == "DOUBLE") {
                    res[i].type = "text";
                    res[i].maxlength = "3";
                    res[i].pattern = "^(([0-9]*)|(([0-9]*)\.([0-9]*)))$" ;
                }
                else if (res[i].type == "URL")
                {

                    res[i].pattern = "((ftp|http|https):\/\/)?" ;
                }
            }
        }).catch(error => {
            console.error(error);
        })
    }

    createStudent() {  // function to create the student Record 
        if (this.recordId == null) {
            console.log("INSIDE SAVE");
            insertStudent({ strData: JSON.stringify(this.objStudent), insertStudent: this.objectName }).then(res => {
                this.Id = res.Id;
                if (this.recordId == null && res.Name == null) {
                    
                    this.showSuccessToastMessage('ERROR', `Enter the required fields`, 'error'); // Shows a toast message when none of the fields/ required fields are entered 

                }
                else if (this.recordId == null) {
                    
                    this.showSuccessToastMessage('SUCCESS', `${res.Name} has been Successfully created`, 'success'); // Shows a success toast message when the required field are entered and the record is created.
                    this.navigateToRecordPage(res.Id);
                }
            }).catch(error => {
                this.showSuccessToastMessage('ERROR', 'Account Not Created', 'error');
                console.error(error);
            })
        }
        else if (this.recordId != null) {
            console.log("INSIDE UPDATE");
            insertStudent({ strData: JSON.stringify(this.studentClone) }).then(res => {
                this.Id = res.Id;
                this.showSuccessToastMessage('SUCCESS', `${res.Name} has been Updated Successfully`, 'success'); // Shows a success toast message when the record data  is updated with new data . 
                this.navigateToRecordPage(res.Id);

            }).catch(error => {
                this.showSuccessToastMessage('ERROR', 'Student Not Created', 'error');
                console.error(error);
            })
        }


    }

    createStudentRecord(event) {
        
        if( event.target.label == "SAVE")
        {
            this.createStudent();
            console.log("SAVE EXECUTED ");
        }
        else if ( event.target.label == "EDIT")
        {
            this.buttonstatus = false;
            this.buttonLabel = "SAVE";
            console.log("EDIT EXECUTED ");
        }
            
           
    }

    inputHandler(event) { // function to retrieve the data from the input fields . 
        var fieldName = event.target.name;
        var fieldValue = event.target.value;
        console.log("PATTERN :" + event.target.pattern);
        console.log("VARIANT :" + event.target.variant);
        console.log("LENGTH :" + event.target.maxlength);
        console.log("TYPE :" + event.target.type);

        if (this.recordId != null) {
            if (event.target.type == "checkbox") {
                this.studentClone[fieldName] = event.target.checked;
            }
            else {
                this.studentClone[fieldName] = fieldValue;
                console.log(this.studentClone[fieldName]);
            }
        }
        else if (this.recordId == null) {
            if (event.target.type == "checkbox") {
                this.objStudent[fieldName] = event.target.checked;
            }
            else {
                this.objStudent[fieldName] = fieldValue;
                console.log(this.objStudent[fieldName]);
            }
        }
    }

    showSuccessToastMessage(title, message, variant) { // function which carries out the toastMessage operation
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    navigateToRecordPage(recordId) { // function which is responsible for the navigation operation. 
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: recordId,
                objectApiName: this.objectApiName,
                actionName: 'view'
            }
        });


    }

    cancelAction(event) { // function which clears the entered data on click in home page and retains the data in record page on click . 
       
        if (this.recordId == null) {
            this.objStudent = {};
            this.template.querySelectorAll('lightning-input').forEach(element => {
                if (element.type === 'checkbox' || element.type === 'checkbox-button') {
                    element.checked = false;
                } else {
                    element.value = null;
                }
            });
        }
        else if (this.recordId != null) {
            console.log('Inside')
            this.buttonLabel ="EDIT";
            this.buttonstatus = true;
           
           let lstTempFields = this.lstFields;
           console.log("STUDENT CLONE"+JSON.stringify(this.studentClone));
            for (var i = 0; lstTempFields.length; i++) {
                console.log("HAS ON PROP STATUS: "+this.studentClone.hasOwnProperty(lstTempFields[i].Name));
                if (this.studentClone.hasOwnProperty(lstTempFields[i].apiName)) 
                {
                    //console.log('>>>>>>>>>>>>>>this.studentClone[lstTempFields[i].Name[i].name',this.studentClone[lstTempFields[i].apiName]);
                    console.log('INside if loop');
                        lstTempFields[i].val = this.studentClone[lstTempFields[i].apiName];
                    }
                    else{ 
                       // console.log('>>>>>>>>>>>>>>lstTempFields[i].apiName>>>>>>>>> ELSE ',lstTempFields[i].apiName);
                        lstTempFields[i].val="";
                    }
                   // console.log('>>>>>>>>>>>>>>>>>>>>>>>>lstTempFields',JSON.parse(JSON.stringify(lstTempFields)));
                }
            }
            this.studentClone = this.studentCloneTwo;

            console.log('studentCloneTwo>>>>>>>>>>>>>',JSON.parse(JSON.stringify(this.studentCloneTwo)));
            console.log('studentClone<<<<<<<<<<<<<<<<<<<<<<<,',JSON.parse(JSON.stringify(this.studentClone)));
            
            this.lstFields = lstTempFields;
            console.log('Temp list'+lstTempFields);
            this.template.querySelectorAll('lightning-input').forEach(element => {
                element.value = this.studentClone.hasOwnProperty(element.name) ? this.studentClone[element.name] : '';
                if(element.type === 'checkbox'){
                    element.checked = this.studentClone[element.name];
                }                      
            });
            
        }




    fetchStudentRecord() { // used to fetch the required record based on the recordId.
        console.log("FETCH ACC");
        studentDetails({ recordId: this.recordId , objectApiName: this.objectName, fieldSet: this.fieldSetName}).then(result => {
            this.studentClone = result;
            this.studentCloneTwo =  result;

          
            for (var i = 0; this.lstFields.length; i++) 
            {
                if (result.hasOwnProperty(this.lstFields[i].apiName)) 
                { 
                    this.lstFields[i].val = result[this.lstFields[i].apiName]
                }

            }

        }).catch(error => {
           
            console.error(error);
        })

    }
   
}


