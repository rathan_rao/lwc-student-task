import { LightningElement, track, api, wire } from 'lwc';
import insertStudent from "@salesforce/apex/StudentHandlerTwo.insertStudent";
import getStudentFieldSetMemebers from "@salesforce/apex/StudentHandlerTwo.getStudentFieldSetMemebers";
import studentDetails from "@salesforce/apex/StudentHandlerTwo.studentDetails";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';

export default class HomePageTwo extends NavigationMixin(LightningElement) {

    @api recordId;
    @api objectApiName;

    @track objStudent = {};
    @track lstStudentTwo = {};
    @track lstFields=[];
    @track buttonstatus = true;
    @track Name;
    @track studentClone = {};
    @track fieldWrapper = {};

    connectedCallback() {  // ConnectedCallback function to call the fieldSetMembers while  the page loads 
        this.fieldSetMember();
    }

    renderedCallback() {  //RenderedCallBack to fetch the accounts . 
        this.fetchStudentRecord();

    }
    fieldSetMember() { // This function retrieves the fields which are set in the field set and displays in the form .
        getStudentFieldSetMemebers({
            objectApiName: 'Student__c',
            fieldsetApiName: 'studentFieldSet'
        }).then(res => {
            this.lstFields = res;
            for (var i = 0; i < res.length; i++) {
                if (res[i].type == "BOOLEAN") {
                    res[i].type = "checkbox";
                    res[i].variant = "label-inline";
                }
                else if (res[i].type == "DOUBLE") {
                    res[i].type = "number";
                    res[i].pattern = "[0-9]";
                    res[i].maxlength= "3";
                }
            }
        }).catch(error => {
            console.error(error);
        })
    }

    createStudent() { // function to create the student Record 
        insertStudent({ strData: JSON.stringify(this.objStudent) }).then(res => {
            this.Id = res.Id;
            if (this.recordId == null && res.Name == null) {
                this.showSuccessToastMessage('ERROR', `Enter the required fields`, 'error'); // Shows a toast message when none of the fields/ required fields are entered 

            }
            else if (this.recordId == null) {
                this.showSuccessToastMessage('SUCCESS', `${res.Name} has been Successfully created`, 'success'); // Shows a success toast message when the required field are entered and the record is created.
                this.navigateToRecordPage(this.recordId);
            }
            else if (this.recordId != null) {
                this.showSuccessToastMessage('SUCCESS', `${res.Name} has been Updated Successfully`, 'success'); // Shows a success toast message when the record data  is updated with new data . 
                this.navigateToRecordPage(this.recordId);
            }

        }).catch(error => {
            this.showSuccessToastMessage('ERROR', 'Account Not Created', 'error');
            console.error(error);
        })
    }

    createStudentRecord() { 
        this.createStudent();
    }

    inputHandler(event) { // function to retrieve the data from the input fields . 
        var fieldName = event.target.name;
        var fieldValue = event.target.value;
        console.log("PATTERN :"+event.target.pattern);
        console.log("VARIANT :"+event.target.variant);
        console.log("LENGTH :"+event.target.maxlength);

        if (this.recordId != null) {
            if (event.target.type == "checkbox") {
                this.studentClone[fieldName] = event.target.checked;
            }
            else {
                this.studentClone[fieldName] = fieldValue;
                console.log(this.studentClone[fieldName]);
            }
        }
        if (event.target.type == "checkbox") {
            this.objStudent[fieldName] = event.target.checked;
        }
        else {
            this.objStudent[fieldName] = fieldValue;
            console.log(this.objStudent[fieldName]);
        }
    }

    showSuccessToastMessage(title, message, variant) { // function which carries out the toastMessage operation
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    navigateToRecordPage(recordId) { // function which is responsible for the navigation operation. 
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: recordId,
                objectApiName: 'Student__c',
                actionName: 'view'
            }
        });


    }

    cancelAction(event) { // function which clears the entered data on click in home page and retains the data in record page on click . 
        if (this.recordId == null) {
            this.objStudent = {};
            this.template.querySelectorAll('lightning-input').forEach(element => {
                if (element.type === 'checkbox' || element.type === 'checkbox-button') {
                    element.checked = false;
                } else {
                    element.value = null;
                }
            });
        }
        else if (this.recordId != null) {
            this.buttonstatus = true;
        console.log("CANCEL CALLED");
        for (var i = 0; this.lstFields.length; i++) {
            for (const [key, value] of Object.entries(this.studentClone)) {
                if (this.lstFields[i].apiName == key) {
                    this.lstFields[i].val = value;
                    console.log("LIST FIELD VALUES :" + this.lstFields[i].val);

                }
            }
        }
        }

        console.log("CANCEL ACTION EXECUTED");


    }

    fetchStudentRecord() { // used to fetch the required record based on the recordId.
        console.log("FETCH ACC");
        studentDetails({ recordId: this.recordId }).then(result => {
            this.studentClone = Object.assign({}, result);
           // this.studentClone = result;
            //console.log("CLONED: "+JSON.stringify(this.studentClone));
            for (var i = 0; this.lstFields.length; i++) {
                for (const [key, value] of Object.entries(result)) {
                    if (this.lstFields[i].apiName == key) {
                        this.lstFields[i].val = value;

                    }
                }

            }

        }).catch(error => {
            console.error(error);
        })

    }

    editForm() { // makes the form editable on click . 
        this.buttonstatus = false;
    }

}


