/*
        @ Class name        :   StudentHandler
        @ Created by        :   Rathan D Rao
        @ Created on        :   03-02-2022
        @ jira ticket       : SLVI -006
        @ Description       :   Create student Record with Save/Edit and Cancel functionality. 
    */


    public with sharing class StudentHandlerTwo {
    
        @AuraEnabled
        public static List<fieldWrapper> getStudentFieldSetMemebers( String objectApiName, String fieldsetApiName) // fucntion to retrieve the fields from  fieldsets from the UI 
        {
    
            List<fieldWrapper> lstFieldWrapper = new List<fieldWrapper>();
            Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectApiName);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
    
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldsetApiName);
    
            List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
            for(Schema.FieldSetMember objFM : fieldSetMemberList )
            {
                lstFieldWrapper.add( new fieldWrapper(objFM.getLabel(), objFM.getFieldPath(), objFM.getRequired(), String.valueOf(objFM.getType()), objFM.getDbRequired() ) );
            }
     
            return lstFieldWrapper; 
        }
        @AuraEnabled
        public static Object insertStudent(String strData)  // function where we carry out the insertion and updation of data . 
        {
            try {
                Student__c objStudent = (Student__c)JSON.deserialize(strData, Student__c.class);
         
                if(String.isNotBlank(objStudent.Id))
                {
                    update objStudent;
                    return new Map<String,String> {'Id' => objStudent.Id, 'Name' =>objStudent.Name};
                }
                else if (String.isNotBlank(objStudent.Name)) {
                    insert objStudent;
                    return new Map<String,String> {'Id' => objStudent.Id, 'Name' =>objStudent.Name};
    
                }
                return objStudent;
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage());
            }
        }
    
        public class fieldWrapper
        {
            @AuraEnabled public String label {get; set;} 
            @AuraEnabled public String apiName {get; set;}
            @AuraEnabled public Boolean isRequired {get; set;}
            @AuraEnabled public String type {get; set;}
            @AuraEnabled public Boolean isDBRequired {get; set;}
    
            fieldWrapper(){}
            fieldWrapper(String label,String apiName, Boolean isRequired,  String type, Boolean isDBRequired  )
            {
                this.label = label;
                this.apiName = apiName;
                this.isRequired = isRequired;
                this.type = type;
                this.isDBRequired = isDBRequired;
            }
        }
    
        @AuraEnabled
        public static Student__c studentDetails(String recordId){ // function to query out the required record we need using the dynamic query method .
            try {
    
                String query = 'SELECT ';
                    for(fieldWrapper fR : getStudentFieldSetMemebers('Student__c','studentFieldSet')) 
                    {
                        query += fR.apiName + ', ';
                    }
                    String sId =  '\''+recordId+'\'';
                    query += '  Id FROM Student__c WHERE Id = '+sId+' ';
                    System.debug('query ------>'+query );
                    Student__c objStudent = Database.query(query);  
                    return  objStudent;     
    
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage());
            }
        }
    
    
    }
    